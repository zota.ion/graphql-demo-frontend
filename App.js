import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import UserList from './src/component/UserList/UserList';
import User from './src/component/UserList/User';
import ApolloWrapper from './src/component/ApolloWrapper';
import AddUser from './src/component/UserList/AddUser';




const AppNavigator = StackNavigator({

  UserList: { screen: ApolloWrapper(UserList)},
  User: { screen: ApolloWrapper(User) },
  AddUser: { screen: ApolloWrapper(AddUser) },

},
  {
    initialRouteName: "UserList",
    headerMode: "none"

  }
);

export default class App extends Component {


  render() {
    return (
      <AppNavigator />
    );
  }
}




