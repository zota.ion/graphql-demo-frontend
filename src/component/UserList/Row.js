import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid } from 'react-native';
import { graphql, ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import findAllUsers from '../../query/FindAllUsers';




class Row extends Component {

  deleteUser = () => {

    this.props.mutate({
      variables: {
        "id": this.props.id,

      },
      refetchQueries: [{ query: findAllUsers }]
    }
    ).then(() => {
      ToastAndroid.show("User Deleted", ToastAndroid.SHORT);
      
    })
      .catch((error) => {
        console.log("Api call error");

        ToastAndroid.show(error.message, ToastAndroid.SHORT);
      });

  }


  render() {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('User', { params: this.props })}
        onLongPress={() => this.deleteUser()} >
        <View key={this.props.id} style={styles.container}>
          <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRI_fCi65LuiROTQlKs3uwpV_aH6tQPCM8hsSQ28gY0-iJgMMNe' }} style={styles.photo} />
          <View>
            <Text style={styles.text}>
              {`name: ${this.props.firstName}`}
            </Text>
            <Text style={styles.text}>
              {`phone: ${this.props.msisdn}`}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    marginLeft: 12,
    fontSize: 16,
  },
  photo: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  button: {
    borderColor: '#8E8E8E',
    borderWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
});

const userDeleteMutation = gql`
    mutation deleteUserMutation($id: ID!) {
    deleteUser(id: $id)
}`;

export default graphql(userDeleteMutation)(Row);