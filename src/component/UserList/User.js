import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, ScrollView, TouchableOpacity, ToastAndroid } from 'react-native';
import { graphql, ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import findAllUsers from '../../query/FindAllUsers';


class User extends Component {

  params = this.props.navigation.getParam('params', '');
  state = {
    firstName: this.params.firstName,
    lastName: this.params.lastName,
    msisdn: this.params.msisdn,
    countryCode: this.params.countryCode,
  };



  updateUser = () => {

    this.props.mutate({
      variables: {
        "id": this.params.id,
        "firstName": this.state.firstName,
        "lastName": this.state.lastName,
        "msisdn": this.state.msisdn,
        "countryCode": this.state.countryCode
      },
      refetchQueries: [{ query: findAllUsers }]
    }
    ).then(() => {
      ToastAndroid.show("User Updated", ToastAndroid.SHORT);
      this.props.navigation.navigate("UserList");
    })
      .catch((error) => {
        console.log("Api call error");

        ToastAndroid.show(error.message, ToastAndroid.SHORT);
      });

  }

  renderUser() {
    return (
      <ScrollView>
        <View style={styles.imageContainer}>
          <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRI_fCi65LuiROTQlKs3uwpV_aH6tQPCM8hsSQ28gY0-iJgMMNe' }} style={styles.photo} />
        </View>
        <View style={styles.container}>
          <TextInput
            style={styles.input}
            placeholder="First Name"
            onChangeText={(text) => { this.setState({ firstName: text }) }}
            value={`${this.state.firstName}`}
          />
        </View>
        <View style={styles.container}>
          <TextInput
            style={styles.input}
            placeholder="Last Name"
            onChangeText={(text) => { this.setState({ lastName: text }) }}
            value={`${this.state.lastName}`}
          />
        </View>
        <View style={styles.container}>
          <TextInput
            style={styles.input}
            placeholder="Phone"
            onChangeText={(text) => { this.setState({ msisdn: text }) }}
            value={`${this.state.msisdn}`}
          />
        </View>
        <View style={styles.container}>
          <TextInput
            style={styles.input}
            placeholder="Country Code"
            onChangeText={(text) => { this.setState({ countryCode: text }) }}
            value={`${this.state.countryCode}`}
          />
        </View>

        <View style={styles.buttonContainer}>

          <TouchableOpacity style={styles.button} onPress={() => this.updateUser()}>
            <Text style={styles.bText}>Save</Text>
          </TouchableOpacity>
        </View>

      </ScrollView>
    );
  }

  render() {

    return this.renderUser();

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageContainer: {
    flex: 3,
    padding: 12,
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: 20,
    paddingVertical: 40,
    backgroundColor: '#42bff4'

  },
  text: {
    marginLeft: 12,
    fontSize: 16,
  },
  photo: {
    height: 100,
    width: 100,
    borderRadius: 50,
  },
  button: {
    borderColor: '#8E8E8E',
    borderWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  input: {
    height: 40,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
  },
  buttonContainer: {
    flex: 1,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 40,
  },

  bText: {
    color: '#8E8E8E',
  },

});

const userUpdateMutation = gql`
    mutation updateUserMutation($id: ID!,$firstName: String!, $lastName: String!, $msisdn: Long!, $countryCode: String!) {
  updateUser(id: $id,firstName: $firstName, lastName: $lastName, msisdn: $msisdn, countryCode: $countryCode){
    id
  }
}`;


export default graphql(userUpdateMutation)(User);