import React, { Component } from 'react';
import { ListView, StyleSheet, Text, View, ScrollView } from 'react-native';
import { graphql, ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import Row from './Row';
import Header from './Header';
import Footer from './Footer';
import findAllUsers from '../../query/FindAllUsers';

class UserList extends Component {


  renderUsers() {

    if (this.props.data.loading) {
      return <Text style={styles.outer}>Loading</Text>;
    } else {

      const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
      this.state = {
        dataSource: ds.cloneWithRows(this.props.data.users),
      };

      return (
        <ScrollView>
          <ListView
            style={styles.container}
            dataSource={this.state.dataSource}
            renderRow={(data) => <Row {...data} navigation={this.props.navigation} />}
            renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
            //renderHeader={() => <Header navigation={this.props.navigation}/>}

            renderFooter={() => <Footer navigation={this.props.navigation} />}
          />
        </ScrollView>
      );
    }

  }
  /*         
   */
  render() {

    return this.renderUsers();

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
  outer: { paddingTop: 32, paddingLeft: 10, paddingRight: 10 },
});


export default graphql(findAllUsers)(UserList);