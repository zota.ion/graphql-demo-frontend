import React, { Component } from 'react';
import { ApolloProvider } from 'react-apollo';
import { ApolloLink } from 'apollo-link';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';


const cache = new InMemoryCache();

const client = new ApolloClient({
     link: new HttpLink({ uri: 'http://172.31.87.105:8080/graphql' }),
    // link: new HttpLink({ uri: 'http://192.168.1.7:8080/graphql' }),    
     cache: cache
   });



export default ApolloWrapper = (CMP) => {
  return class extends Component {

    render() {
      return (
        <ApolloProvider client={client}>
          <CMP {...this.props} />
        </ApolloProvider>
      );
    }
  };
}