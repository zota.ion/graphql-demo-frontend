import gql from 'graphql-tag';

export default gql`
query allUsers {
    users: findAllUsers(size: 100) {
      firstName
      lastName
      msisdn
      countryCode
      id
    }
  }
`